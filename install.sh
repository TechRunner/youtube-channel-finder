#!/bin/bash

echo "Installing Youtube-Search"

# Check to see if user is root
if [ $(whoami) != "root" ]; then
    echo "User is not root please user root to install"
else
    # Copy Python file to /usr/bin/
    if [ ! -f /usr/bin/youtube-search ]; then
        cp Youtube-Search.py /usr/bin/youtube-search
        chmod +x /usr/bin/youtube-search
    else
        echo "Application already exists do you want to continue[y/N]: "
        read answer
        if [ answer == "y" ]; then
            cp Youtube-Search.py /usr/bin/youtube-search
            echo "Done!"
        fi
    fi
fi

