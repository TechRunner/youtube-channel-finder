#!/usr/bin/env python3
import sys
import os
from bs4 import BeautifulSoup as soup
from urllib.request import urlopen as uReq

def FindChannel(name):
    # Grab site search
    site = uReq(f"https://www.youtube.com/results?search_query={name}&sp=EgIQAg%253D%253D")
    
    # Read webpage
    site_html = site.read()

    # Close connection
    site.close()

    # Turn html into something usable
    site_soup = soup(site_html, "html.parser")

    # Extract useful section
    container = site_soup.findAll("div", {"class":"yt-lockup-content"})


    for x in range(len(container)-1):
        # Extract Channel Name
        channel_name = container[x].h3.a.text

        # Extract Url
        channel = container[x].h3.a['href']
        channel_url = f"https://www.youtube.com{channel}"

        # Extract Description
        description = container[x].findAll("div")[1].text

        # Extract Subscriber Count
        subscriber_count = container[x].find("span", {"class":"yt-subscriber-count"}).text

        # Print everything out
        print("-"*80)
        print(f"Channel #{x}")
        print(f"\tChannel: {channel_name}\n\tURL: {channel_url}\n\tDescription: {description}\n\tSubscriber Count: {subscriber_count}")
        # Start Youtube-Viewer after
    channel_select = int(input("Which channel do you want to watch[-1 to exit]: "))
    if channel_select in range(len(container)):
        os.system(f"firefox https://www.youtube.com{container[channel_select].h3.a['href']}")
    elif channel_select == -1:
        print("")
    else:
        print("You selected something that does not exist")


# Help Menu
def PrintHelp():
    print("""Usage: Youtube-Search [OPTION]... [Channel Name]
            Lists out channels found on the first page of Youtubes search with the channel filter set
            -h, --help          Displays this help and exits""")

if __name__ == "__main__":
    run = True
    # Print flag
    if "-h" in sys.argv or "--help" in sys.argv:
        PrintHelp()
        run = False

    if len(sys.argv) > 1 and run:
        count = FindChannel(sys.argv[-1])

    

